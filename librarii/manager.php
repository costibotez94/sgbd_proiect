<?php

class Manager {
	private static $server = "localhost";
	private static $username = "sgbd_usr";
	private static $password = "sgbd";
	private static $baze_de_date_interzise = ['performance_schema', 'licenta', 'phpmyadmin', 'mysql', 'information_schema'];

	public static function adaugareBazaDeDate($nume_baza_de_date) {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}


		$query = "CREATE DATABASE $nume_baza_de_date";
		if(mysqli_query($link, $query)) {
			echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-au adaugat baza de date cu numele ' . $nume_baza_de_date . '!</div>';
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);
	}

	public static function afiseazaBazeDeDate($form_name="adaugare_tabel") {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}


		$query = "SHOW DATABASES;";
		$result = mysqli_query($link, $query);
		// print_r($result); exit;
		if($result->num_rows !== 0) { ?>
			<select class="form-control" name="baza_de_date" form="<?php echo $form_name; ?>">
				<option value="0">Alege baza de date...</option>
				<?php while($row = $result->fetch_array()) : ?>
					<?php if(!in_array($row['Database'], Manager::$baze_de_date_interzise)) : ?>
						<option value="<?php echo $row['Database']; ?>"><?php echo $row['Database']; ?></option>
					<?php endif; ?>
				<?php endwhile; ?>
			</select>
			<?php
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);
	}

	public static function afiseazaTabele($form_name = "adaugare_rand") {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$query = "SHOW DATABASES";
		$result = mysqli_query($link, $query);

		$baze_de_date = $tabele = [];
		$k=0;

		if($result->num_rows !== 0) {
			while($row = $result->fetch_array()) {
				if(!in_array($row['Database'], Manager::$baze_de_date_interzise)) {
					$baze_de_date[] = $row['Database'];
				}
			}


			foreach ($baze_de_date as $baza_de_date) {
				$query = "SHOW TABLES";
				if(mysqli_select_db($link, $baza_de_date)) {
					$result = mysqli_query($link, $query);

					if($result->num_rows !== 0) {
						while($row = $result->fetch_array()) {
							$tabele[$k][0] = $baza_de_date;
							$tabele[$k][1] = $row[0];
							$k++;
							// $tabele[] = $row[0];
						}
					}
				}
			} ?>

			<select class="form-control" name="tabele" form="<?php echo $form_name; ?>">
				<option value="0">Alege tabelul...</option>
				<?php foreach ($tabele as $tabel) : ?>
					<?php if(isset($_POST['tabele']) && $tabel[0] . '-' . $tabel[1] == $_POST['tabele']) : ?>
						<option value="<?php echo $tabel[0] . '-' . $tabel[1]; ?>" selected><?php echo $tabel[0] . '-' . $tabel[1]; ?></option>
					<?php else : ?>
						<option value="<?php echo $tabel[0] . '-' . $tabel[1]; ?>"><?php echo  $tabel[0] . '-' . $tabel[1]; ?></option>
					<?php endif; ?>
				<?php endforeach; ?>
			</select>
			<?php
			//echo '<pre>'; print_r($tabele); echo '</pre>';
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);
	}

	public static function adaugareTabel($baza_de_date, $nume_tabel, $coloane) {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		if(mysqli_select_db($link, $baza_de_date)) {
			$query = "CREATE TABLE $nume_tabel( id int NOT NULL AUTO_INCREMENT, ";
			$vector_coloane = explode("|", preg_replace('/\s+/', '', $coloane));
			$i=0;
			foreach ($vector_coloane as $coloana) {
				$vector_coloana = explode(":", $coloana);

				$query .= $vector_coloana[0] . ' ';

				$tip_de_data = substr($vector_coloana[1], 0, 1);
				switch($tip_de_data) {
					case 'i':
						$query .= 'int';
					break;
					case 'v':
						$marime = substr($vector_coloana[1], 1);
						$query .= "varchar($marime)";
					break;
					case 'b':
						$query .= 'boolean';
					break;
					case 'f':
						$query .= 'float';
					break;
					case 'r':
						$query .= 'real';
					break;
				}
				$i++;
				if($i<=count($vector_coloane)-1)
					$query .= ', ';
			}
			$query .= ', PRIMARY KEY (id))';
			// echo $query;
			if(mysqli_query($link, $query)) {
				echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-au adaugat tabelul cu numele ' . $nume_tabel . '!</div>';
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
			}
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);
	}

	public static function adaugareRand($nume_tabel, $form_name="adaugare_rand_final"){
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$nume_tabel = explode('-', $nume_tabel);
		$tipuri_de_date_disponibile = ['int', 'varchar'];

		if(mysqli_select_db($link, $nume_tabel[0])) {
			$query = "DESCRIBE $nume_tabel[1]";
			$result = mysqli_query($link, $query);
			if($result->num_rows !== 0) {
				while($row = $result->fetch_array()) {
					$gasit = 0;
					foreach($tipuri_de_date_disponibile as $tip)
						if(strpos($row['Type'], $tip) !== FALSE)
							$gasit = $tip;

					if($gasit !== 0) {
						if($gasit == 'int') {
							echo '<input type="number" name="' . $row['Field'] . '-' . $row['Type'] . '" class="form-control" required placeholder="' . $row['Field'] . '" form="' . $form_name . '" />';
						}
						elseif($gasit == 'varchar') {
							echo '<input type="text" name="' . $row['Field']. '-' . $row['Type'] . '" class="form-control" required placeholder="' . $row['Field'] . '" form="' . $form_name . '" />';
						}
					}
				}
				echo '<div class="submit-form">';
				echo 	'<input type="hidden" name="nonce" value="' . $form_name . '" form="' . $form_name . '"/>';
				echo 	'<input type="hidden" name="tabel" value="' . $nume_tabel[1] . '" form="' . $form_name . '"/>';
				echo 	'<input type="hidden" name="baza_de_date" value="' . $nume_tabel[0] . '" form="' . $form_name . '"/>';
				echo 	'<input type="submit" name="submit" class="btn btn-primary" value="ADAUGA" form="' . $form_name . '"/>';
				echo '</div>';
				echo '<form method="post" id="' . $form_name . '"></form>';
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
			}
		}

		mysqli_close($link);
	}

	public static function adaugareRandFinal() {
		$coloane = "(";

		foreach ($_POST as $key => $value) {
			if($key != 'nonce') {
				$key = explode('-', $key);
				$coloane .= $key[0] . ",";
			}
			else {
				$coloane = rtrim($coloane, ',');
				break;
			}
		}
		$coloane .= ")";

		// INSERT INTO test(id, nume, prenume) VALUES(1, "2", "3");


		$query = "INSERT INTO $_POST[tabel]$coloane VALUES (";
		foreach ($_POST as $key => $value) {
			if(!in_array($key, array('nonce', 'submit', 'tabel'))) {
				$key = explode('-', $key);
				if(strpos($key[1], 'int') !== FALSE)
					$query .= $value . ',';
				else
					$query .= "\"" . $value . '",';
			} else {
				$query = rtrim($query, ',');
				break;
			}
		}
		$query .= ')';

		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		if(mysqli_select_db($link, $_POST['baza_de_date'])) {
			if(mysqli_query($link, $query)) {
				echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-au adaugat un rand in tabelul ' . $_POST['tabel'] . '!</div>';
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
			}
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);

		// echo '<pre>'; print_r($query); exit;
	}

	// ****************************EDITARE********************************

	public static function editareBazaDeDate($nume_vechi, $nume_nou) {
		$link1 = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);
		$link2 = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		mysqli_query($link1, "CREATE DATABASE $nume_nou");
		mysqli_select_db($link1, $nume_nou);
		set_time_limit(0);

		mysqli_select_db($link2, $nume_vechi);

		$tables = mysqli_query($link1, "SHOW TABLES FROM $nume_vechi");

		while ($line = $tables->fetch_row()) {
		    $tab = $line[0];
		    mysqli_query($link1, "DROP TABLE IF EXISTS $nume_nou.$tab");
		    mysqli_query($link1, "CREATE TABLE $nume_nou.$tab LIKE $nume_vechi.$tab") or die(mysqli_error($link));
		}

		mysqli_query($link1, "DROP DATABASE $nume_vechi");

		mysqli_close($link1);
		mysqli_close($link2);
	}

	public static function editareRand($nume_tabel, $form_name="adaugare_rand_final"){
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$nume_tabel = explode('-', $nume_tabel);
		$tipuri_de_date_disponibile = ['int', 'varchar'];

		if(mysqli_select_db($link, $nume_tabel[0])) {
			$query = "SELECT * FROM $nume_tabel[1]";
			$result = mysqli_query($link, $query);
			if($result->num_rows !== 0) {  ?>
			<?php  $row = $result->fetch_array(); ?>
				<table class="table table-hover">
					<thead>
						<?php  foreach (array_keys($row) as $key) : ?>
							<?php if(!is_numeric($key)) :  ?>
								<th><?php echo $key;  ?></th>
							<?php endif; ?>
						<?php endforeach; ?>
					</thead>
					<tbody>
				<?php while($row) : ?>
						<tr>
							<?php  foreach (array_keys($row) as $key) : ?>
								<?php if(!is_numeric($key)) :  ?>
									<td><?php echo $row[$key];  ?></td>
								<?php endif; ?>
							<?php endforeach; ?>
						</tr>
					<?php $row = $result->fetch_array(); ?>
				<?php endwhile; ?>
					</tbody>
				</table>
				<?php if($form_name=="adaugare_rand_final" || $form_name=="editare_rand_final") : ?>
				<div>
					<label for="id_coloana">Introduceti id-ul: </label>
					<input type="numeric" name="id_coloana" id="id_coloana" class="form-control" form="cauta-inregistrare"/>
					<input type="submit" name="submit" value="CAUTA" class="form-control" form="cauta-inregistrare"/>
					<input type="hidden" name="nonce" value="cauta-inregistrare" form="cauta-inregistrare"/>
					<input type="hidden" name="tabele" value="<?php echo $_POST['tabele']; ?>" form="cauta-inregistrare"/>
				</div>
				<form method="POST" id="cauta-inregistrare"></form>
			<?php endif; ?>
				<?php
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> Tabelul nu are randuri!</div>';
			}
		}

		mysqli_close($link);
	}

	public static function cauta_inregistrare($nume_tabel, $id_coloana) {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$nume_tabel = explode('-', $nume_tabel);
		$tipuri_de_date_disponibile = ['int', 'varchar'];

		if(mysqli_select_db($link, $nume_tabel[0])) {
			$query = "SELECT * FROM $nume_tabel[1] WHERE id=$id_coloana";
			$result = mysqli_query($link, $query);
			if($result->num_rows !== 0) {  ?>
				<?php while($row = $result->fetch_array()) : ?>
					<?php  foreach (array_keys($row) as $key) : ?>
						<?php if(!is_numeric($key) && $key != 'id') :  ?>
							<div>
								<label for="<?php echo $key; ?>"><?php echo $key; ?></label>
								<input type="text" name="update[<?php echo $key; ?>]" placeholder="<?php echo $row[$key]; ?>" class="form-control" value="<?php echo $row[$key]; ?>" form="actualizare-inregistrare"/>
							</div>
						<?php elseif(!is_numeric($key)) : ?>
							<div>
								<label for="<?php echo $key; ?>"><?php echo $key; ?></label>
								<input type="text" name="update[<?php echo $key; ?>]" placeholder="<?php echo $row[$key]; ?>" class="form-control" value="<?php echo $row[$key]; ?>" form="actualizare-inregistrare" disabled/>
								<input type="hidden" name="update[<?php echo $key; ?>]" value="<?php echo $row[$key]; ?>" form="actualizare-inregistrare"/>
							</div>
						<?php endif; ?>
					<?php endforeach; ?>
				<?php endwhile; ?>
				<div>
					<input type="submit" name="submit" value="ACTUALIZARE" class="form-control" form="actualizare-inregistrare"/>
					<input type="hidden" name="nonce" value="actualizare-inregistrare" form="actualizare-inregistrare"/>
					<input type="hidden" name="tabele" value="<?php echo $_POST['tabele']; ?>" form="actualizare-inregistrare"/>
				</div>
				<form method="POST" id="actualizare-inregistrare"></form>
				<?php
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> Tabelul nu are randuri!</div>';
			}
		}

		mysqli_close($link);
	}

	public static function editareRandFinal() {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$tabel = explode('-', $_POST['tabele'])[1];
		$baza_de_date = explode('-', $_POST['tabele'])[0];
		// echo '<pre>'; print_r($_POST);
		$query = "UPDATE $tabel SET ";

		$i = 0;
		foreach ($_POST['update'] as $key => $value) {
			$i++;
			if($key != 'id') {
				if($i <= count($_POST['update'])-1)
					$query .= $key . '="' . $value . '",';
				else
					$query .= $key . '="' . $value . '"';
			}
		}

		$query .= ' WHERE id = ' .  $_POST['update']['id'];
		if(mysqli_select_db($link, $baza_de_date)) {
			$result = mysqli_query($link, $query);

			if($result) {
				echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-a actualizat tabelul ' . $tabel . '!</div>';
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
			}
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}
	}

	public static function editare_tabel_sterge_coloana($lista_tabele, $nume_coloana) {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$tabel = explode('-', $_POST['tabele'])[1];
		$baza_de_date = explode('-', $_POST['tabele'])[0];

		$query = "ALTER TABLE $tabel DROP COLUMN $nume_coloana";

		if(mysqli_select_db($link, $baza_de_date)) {
			$result = mysqli_query($link, $query);

			if($result) {
				echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-a actualizat tabelul ' . $tabel . '!</div>';
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
			}
		}

	}

	public static function editare_tabel_adauga_coloana($lista_tabele, $nume_coloana, $tip_de_data) {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$tabel = explode('-', $_POST['tabele'])[1];
		$baza_de_date = explode('-', $_POST['tabele'])[0];

		$query = "ALTER TABLE $tabel ADD COLUMN $nume_coloana $tip_de_data";

		if(mysqli_select_db($link, $baza_de_date)) {
			$result = mysqli_query($link, $query);

			if($result) {
				echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-a actualizat tabelul ' . $tabel . '!</div>';
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
			}
		}
	}

	// ****************************STERGERE********************************

	public static function stergereBazaDeDate() {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$query = "DROP DATABASE $_POST[baza_de_date]";
		if(mysqli_query($link, $query)) {
			echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-au sters baza de date ' . $_POST['baza_de_date'] . '!</div>';
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);
	}

	public static function stergereTabel($tabele) {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$tabele = explode("-", $tabele);
		if(mysqli_select_db($link, $tabele[0])) {
			$query = "DROP TABLE $tabele[1]";
			if(mysqli_query($link, $query)) {
				echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-a sters tabelul ' . $tabele[1] . '!</div>';
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
			}
		}

		mysqli_close($link);
	}

	public static function afisareTabeleStergere($nume_tabel, $form_name="adaugare_rand_final") {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$nume_tabel = explode('-', $nume_tabel);
		$tipuri_de_date_disponibile = ['int', 'varchar'];

		if(mysqli_select_db($link, $nume_tabel[0])) {
			$query = "SELECT * FROM $nume_tabel[1]";
			$result = mysqli_query($link, $query);
			if($result->num_rows !== 0) {  ?>
			<?php  $row = $result->fetch_array(); ?>
				<table class="table table-hover">
					<thead>
						<?php  foreach (array_keys($row) as $key) : ?>
							<?php if(!is_numeric($key)) :  ?>
								<th><?php echo $key;  ?></th>
							<?php endif; ?>
						<?php endforeach; ?>
					</thead>
					<tbody>
				<?php while($row) : ?>
						<tr>
							<?php  foreach (array_keys($row) as $key) : ?>
								<?php if(!is_numeric($key)) :  ?>
									<td><?php echo $row[$key];  ?></td>
								<?php endif; ?>
							<?php endforeach; ?>
						</tr>
					<?php $row = $result->fetch_array(); ?>
				<?php endwhile; ?>
					</tbody>
				</table>
				<div>
					<label for="id_coloana">Introduceti id-ul: </label>
					<input type="numeric" name="id_coloana" id="id_coloana" class="form-control" form="sterge-inregistrare"/>
					<input type="submit" name="submit" value="CAUTA" class="form-control" form="sterge-inregistrare"/>
					<input type="hidden" name="nonce" value="sterge-inregistrare" form="sterge-inregistrare"/>
					<input type="hidden" name="tabele" value="<?php echo $_POST['tabele']; ?>" form="sterge-inregistrare"/>
				</div>
				<form method="POST" id="sterge-inregistrare"></form>
				<?php
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> Tabelul nu are randuri!</div>';
			}
		}

		mysqli_close($link);
	}

	public static function stergereRand($nume_tabel, $id_coloana){
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$nume_tabel = explode('-', $nume_tabel);
		$tipuri_de_date_disponibile = ['int', 'varchar'];

		if(mysqli_select_db($link, $nume_tabel[0])) {
			$query = "DELETE FROM $nume_tabel[1] WHERE id=$id_coloana";
			// echo $query;
			if(mysqli_query($link, $query)) {
				echo '<div class="alert alert-info" role="alert"><strong>Succes!</strong> S-a sters randul ' . $id_coloana . '!</div>';
			} else {
				echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
			}
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);
	}

	// ************************LISTARE***********************************

	public static function listareBazeDeDate() {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$query = "SHOW DATABASES";
		$result = mysqli_query($link, $query);
		if($result->num_rows!==0) { ?>
			<table class="table table-hover">
				<thead>
					<th>Nume</th>
				</thead>
				<?php while( $row = $result->fetch_array()) : ?>
					<?php if(!in_array($row['Database'], Manager::$baze_de_date_interzise)) : ?>
					<tr>
						<td><?php echo $row['Database']; ?></td>
					</tr>
					<?php endif; ?>
				<?php endwhile; ?>
			</table>
			<?php
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);
	}

	public static function listareTabele() {
		$link = mysqli_connect(Manager::$server, Manager::$username, Manager::$password);

		if (!$link) {
		    die('Eșec la conectare: ' . mysqli_error($link));
		}

		$query = "SHOW DATABASES";
		$result = mysqli_query($link, $query);

		$baze_de_date = $tabele = [];
		$k=0;

		if($result->num_rows !== 0) {
			while($row = $result->fetch_array()) {
				if(!in_array($row['Database'], Manager::$baze_de_date_interzise)) {
					$baze_de_date[] = $row['Database'];
				}
			}

			foreach ($baze_de_date as $baza_de_date) {
				$query = "SHOW TABLES";
				if(mysqli_select_db($link, $baza_de_date)) {
					$result = mysqli_query($link, $query);

					if($result->num_rows !== 0) {
						while($row = $result->fetch_array()) {
							$tabele[$k][0] = $baza_de_date;
							$tabele[$k][1] = $row[0];
							$k++;
							// $tabele[] = $row[0];
						}
					}
				}
			} ?>
			<table class="table table-hover">
				<thead>
					<th>Tabele</th>
				</thead>
				<tbody>
				<?php foreach ($tabele as $tabel) : ?>
					<tr>
						<td><?php echo $tabel[1]; ?></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<?php
			//echo '<pre>'; print_r($tabele); echo '</pre>';
		} else {
			echo '<div class="alert alert-danger" role="alert"><strong>Greseala!</strong> ' . mysqli_error($link) . '</div>';
		}

		mysqli_close($link);
	}

}

?>