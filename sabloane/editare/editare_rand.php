<div class="col-md-12">
	<div class="panel panel-warning">
		<div class="panel-heading">Editare rand</div>
		<div class="panel-body">
			<?php Manager::afiseazaTabele('editare_rand'); ?>

			<div class="submit-form">
				<input type="hidden" name="nonce" value="editare_rand" form="editare_rand"/>
				<input type="submit" name="submit" class="btn btn-secondary" value="EDITEAZA" form="editare_rand"/>
			</div>
			<form method="post" id="editare_rand"></form>
			<?php
				if($_POST)
				{
					// Imi afiseaza tabelele
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'editare_rand')
					{
						Manager::editareRand($_POST['tabele'], 'editare_rand_final');
					}

					//Imi
					// if(!empty($_POST['nonce']) && $_POST['nonce'] == 'editare_rand_final')
					// {
					// 	Manager::adaugareRandFinal();
					// }

					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'cauta-inregistrare')
					{
					// echo '<pre>'; print_r($_POST); echo '</pre>';
						Manager::editareRand($_POST['tabele'], 'editare_rand_final');
						Manager::cauta_inregistrare($_POST['tabele'], $_POST['id_coloana']);
					}

					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'actualizare-inregistrare')
					{
						Manager::editareRandFinal();
					}


				}
			?>
		</div>
	</div>
</div>