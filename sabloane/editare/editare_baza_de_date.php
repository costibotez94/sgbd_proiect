<div class="col-md-12">
	<div class="panel panel-warning">
		<div class="panel-heading">Editare baza de date</div>
		<div class="panel-body">
			<?php Manager::afiseazaBazeDeDate('editare_baza_de_date'); ?>
			<div>
				<label for="nume_baza_de_date">Nume:</label>
				<input type="text" name="nume_baza_de_date" id="nume_baza_de_date" class="form-control" required form="editare_baza_de_date"/>
			</div>

			<div class="submit-form">
				<input type="hidden" name="nonce" value="editare_baza_de_date" form="editare_baza_de_date"/>
				<input type="submit" name="submit" class="btn btn-secondary" value="EDITEAZA" form="editare_baza_de_date"/>
			</div>
			<form method="post" id="editare_baza_de_date"></form>
			<?php
				if($_POST)
				{
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'editare_baza_de_date')
					{
						Manager::editareBazaDeDate($_POST['baza_de_date'], $_POST['nume_baza_de_date']);
					}
				}
			?>
		</div>
	</div>
</div>