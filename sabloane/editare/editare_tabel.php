<div class="col-md-12">
	<div class="panel panel-warning">
		<div class="panel-heading">Editare tabel</div>
		<div class="panel-body">
			<?php Manager::afiseazaTabele('editare_tabel'); ?>

			<div class="submit-form">
				<input type="hidden" name="nonce" value="editare_tabel" form="editare_tabel"/>
				<input type="submit" name="submit" class="btn btn-secondary" value="EDITEAZA" form="editare_tabel"/>
			</div>
			<form method="post" id="editare_tabel"></form>
			<?php
				if($_POST)
				{
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'editare_tabel'): ?>
						<form method="post" id="adauga_coloana">
							<input type="hidden" name="tabele" value="<?php echo $_POST['tabele']; ?>" />
							<input type="hidden" name="nonce" value="adauga_coloana" form="adauga_coloana"/>
							<input type="submit" name="adauga_coloana" class="btn btn-secondary" value="ADAUGA COLOANA" form="adauga_coloana">
						</form>
						<form method="post" id="sterge_coloana">
							<input type="hidden" name="tabele" value="<?php echo $_POST['tabele']; ?>" />
							<input type="hidden" name="nonce" value="sterge_coloana" form="sterge_coloana"/>
							<input type="submit" name="submit" class="btn btn-secondary" value="STERGE COLOANA" form="sterge_coloana">
						</form>

						<?php
					endif;

					// adaugare coloana
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'adauga_coloana') : ?>
						<form method="post" id="nume_coloana">
							<input type="text" name="nume_coloana" placeholder="Nume coloana" class="form-control" form="nume_coloana">
							<select name="tip_de_date" class="form-control" form="nume_coloana">
								<option value="">Alege</option>
								<option value="date">Date</option>
								<option value="int">Int</option>
								<option value="float">Float</option>
								<option value="varchar">Varchar</option>
							</select>
							<input type="hidden" name="tabele" value="<?php echo $_POST['tabele']; ?>" form="nume_coloana"/>
							<input type="hidden" name="nonce" value="nume_coloana" form="nume_coloana"/>
							<input type="submit" name="submit" class="btn btn-secondary" value="ADAUGA COLOANA" form="nume_coloana">
						</form>
					<?php
					endif;

					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'nume_coloana') :
						Manager::editare_tabel_adauga_coloana($_POST['tabele'], $_POST['nume_coloana'], $_POST['tip_de_date']);
					endif;

					// stergere coloana
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'sterge_coloana') : ?>
						<form method="post" id="nume_coloana_stergere">
							<input type="text" name="nume_coloana" placeholder="Nume coloana" class="form-control" form="nume_coloana_stergere">
							<input type="hidden" name="tabele" value="<?php echo $_POST['tabele']; ?>" form="nume_coloana_stergere"/>
							<input type="hidden" name="nonce" value="nume_coloana_stergere" form="nume_coloana_stergere"/>
							<input type="submit" name="submit" class="btn btn-secondary" value="STERGE COLOANA" form="nume_coloana_stergere">
						</form>
					<?php
					endif;

					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'nume_coloana_stergere') :
						Manager::editare_tabel_sterge_coloana($_POST['tabele'], $_POST['nume_coloana']);
					endif;

				}
			?>
		</div>
	</div>
</div>