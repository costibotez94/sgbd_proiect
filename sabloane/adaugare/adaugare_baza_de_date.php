<div class="col-md-12">
	<div class="panel panel-primary">
		<div class="panel-heading">Adaugare baza de date</div>
		<div class="panel-body">
			<div>
				<label for="nume_baza_de_date">Nume:</label>
				<input type="text" name="nume_baza_de_date" id="nume_baza_de_date" class="form-control" required form="adaugare_baza_de_date"/>
			</div>
			
			<div class="submit-form">
				<input type="hidden" name="nonce" value="adaugare_baza_de_date" form="adaugare_baza_de_date"/>
				<input type="submit" name="submit" class="btn btn-primary" value="ADAUGA" form="adaugare_baza_de_date"/>
			</div>
			<form method="post" id="adaugare_baza_de_date"></form>
			<?php
				if($_POST)
				{
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'adaugare_baza_de_date')
					{
						Manager::adaugareBazaDeDate($_POST['nume_baza_de_date']);
					}
				}
			?>
		</div>
	</div>
</div>