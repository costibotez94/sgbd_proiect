<div class="col-md-12">
	<div class="panel panel-primary">
		<div class="panel-heading">Adaugare tabel</div>
		<div class="panel-body">
			<?php Manager::afiseazaBazeDeDate(); ?>
			<div>
				<label for="nume_tabel">Nume tabel:</label>
				<input type="text" name="nume_tabel" id="nume_tabel" class="form-control"  required form="adaugare_tabel"/>
			</div>
			<div>
				<label for="coloane_tabel">Coloane:</label>
				<input type="text" name="coloane_tabel" id="coloane_tabel" class="form-control" required form="adaugare_tabel"/>
			</div>
			<div class="descriere">Folositi o expresie regulata pentru a crea tabelul (vezi Meniul Help) (ex. nume:v200 | prenume:v20 | cnp:v13)</div>
			<div class="submit-form">
				<input type="hidden" name="nonce" value="adaugare_tabel" form="adaugare_tabel"/>
				<input type="submit" name="submit" class="btn btn-primary" value="ADAUGA" form="adaugare_tabel"/>
			</div>
			<form method="post" id="adaugare_tabel"></form>
			<?php
				if($_POST)
				{
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'adaugare_tabel')
					{
						
						Manager::adaugareTabel($_POST['baza_de_date'], $_POST['nume_tabel'], $_POST['coloane_tabel']);
					}
				}
			?>
		</div>
	</div>
</div>