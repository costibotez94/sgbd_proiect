<div class="col-md-12">
	<div class="panel panel-primary">
		<div class="panel-heading">Adaugare rand</div>
		<div class="panel-body">
			<?php Manager::afiseazaTabele(); ?>
			
			<div class="submit-form">
				<input type="hidden" name="nonce" value="adaugare_rand" form="adaugare_rand"/>
				<input type="submit" name="submit" class="btn btn-primary" value="ADAUGA" form="adaugare_rand"/>
			</div>
			<form method="post" id="adaugare_rand"></form>
			<?php
				if($_POST)
				{
					// echo '<pre>'; print_r($_POST); echo '</pre>';
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'adaugare_rand')
					{
						Manager::adaugareRand($_POST['tabele']);
					}

					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'adaugare_rand_final')
					{
						Manager::adaugareRandFinal();
					}
				}
			?>
		</div>
	</div>
</div>