<div class="col-md-12">
	<div class="panel panel-danger">
		<div class="panel-heading">Stergere tabel</div>
		<div class="panel-body">
			<?php Manager::afiseazaTabele('stergere_tabel'); ?>
			
			<div class="submit-form">
				<input type="hidden" name="nonce" value="stergere_tabel" form="stergere_tabel"/>
				<input type="submit" name="submit" class="btn btn-secondary" value="STERGE" form="stergere_tabel"/>
			</div>
			<form method="post" id="stergere_tabel"></form>
			<?php
				if($_POST)
				{
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'stergere_tabel')
					{
						Manager::stergereTabel($_POST['tabele']);
					}
				}
			?>
		</div>
	</div>
</div>