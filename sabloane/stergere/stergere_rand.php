<div class="col-md-12">
	<div class="panel panel-danger">
		<div class="panel-heading">Stergere rand</div>
		<div class="panel-body">
			<?php Manager::afiseazaTabele('stergere_rand'); ?>
			
			<div class="submit-form">
				<input type="hidden" name="nonce" value="stergere_rand" form="stergere_rand"/>
				<input type="submit" name="submit" class="btn btn-secondary" value="ADAUGA" form="stergere_rand"/>
			</div>
			<form method="post" id="stergere_rand"></form>
			<?php
				if($_POST)
				{
					// Imi afiseaza tabelele
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'stergere_rand')
					{
						Manager::afisareTabeleStergere($_POST['tabele'], 'stergere_rand');
					}


					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'sterge-inregistrare')
					{
						Manager::stergereRand($_POST['tabele'], $_POST['id_coloana']);
					}
				}
			?>
		</div>
	</div>
</div>