<div class="col-md-12">
	<div class="panel panel-danger">
		<div class="panel-heading">Stergere baza de date</div>
		<div class="panel-body">
			<?php Manager::afiseazaBazeDeDate('stergere_baza_de_date'); ?>
			
			<div class="submit-form">
				<input type="hidden" name="nonce" value="stergere_baza_de_date" form="stergere_baza_de_date"/>
				<input type="submit" name="submit" class="btn btn-secondary" value="STERGE" form="stergere_baza_de_date"/>
			</div>
			<form method="post" id="stergere_baza_de_date"></form>
			<?php
				if($_POST)
				{
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'stergere_baza_de_date')
					{
						Manager::stergereBazaDeDate();
					}
				}
			?>
		</div>
	</div>
</div>