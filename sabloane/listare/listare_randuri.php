<div class="col-md-12">
	<div class="panel panel-success">
		<div class="panel-heading">Listare randuri</div>
		<div class="panel-body">
			<?php Manager::afiseazaTabele('listare_randuri'); ?>

			<div class="submit-form">
				<input type="hidden" name="nonce" value="listare_randuri" form="listare_randuri"/>
				<input type="submit" name="submit" class="btn btn-secondary" value="LISTEAZA" form="listare_randuri"/>
			</div>
			<form method="post" id="listare_randuri"></form>
			<?php
				if($_POST)
				{
					// echo '<pre>'; print_r($_POST); echo '</pre>';
					if(!empty($_POST['nonce']) && $_POST['nonce'] == 'listare_randuri')
					{
						Manager::editareRand($_POST['tabele'], 'listare_randuri_final');
					}

					// if(!empty($_POST['nonce']) && $_POST['nonce'] == 'listare_randuri_final')
					// {
					// 	Manager::adaugareRandFinal();
					// }
				}
			?>
		</div>
	</div>
</div>