-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 03, 2016 at 06:09 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `licenta`
--

-- --------------------------------------------------------

--
-- Table structure for table `discipline`
--

CREATE TABLE `discipline` (
  `id` int(11) NOT NULL,
  `cod` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `nume` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `evaluare` enum('E','C') COLLATE utf8_romanian_ci NOT NULL DEFAULT 'E',
  `tip` enum('Curs','Seminar','Laborator') COLLATE utf8_romanian_ci NOT NULL DEFAULT 'Curs',
  `optional` int(11) NOT NULL DEFAULT '0',
  `semestru` int(11) NOT NULL,
  `an` int(11) NOT NULL,
  `specializare_id` int(11) NOT NULL,
  `credite` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `discipline`
--

INSERT INTO `discipline` (`id`, `cod`, `nume`, `evaluare`, `tip`, `optional`, `semestru`, `an`, `specializare_id`, `credite`) VALUES
(1, 'FMI.Info.I.1.01', 'Analiza I: Calcul diferential si integral', 'E', 'Curs', 1, 1, 2, 1, 6),
(2, 'FMI.Info.I.1.02', 'Algoritmi si structuri de date I', 'E', 'Curs', 1, 1, 1, 1, 6),
(3, 'FMI.Info.I.1.03', 'Programare procedurala', 'E', 'Curs', 1, 1, 1, 1, 6),
(4, 'FMI.Info.I.1.04', 'Algebra (Algebra liniara)', 'E', 'Curs', 0, 2, 1, 1, 5),
(5, 'FMI.Info.I.1.05', 'Arhitectura sistemelor de calcul', 'E', 'Curs', 0, 1, 1, 1, 6),
(6, 'FMI.Info.I.1.11', 'Limbi straine I (engleza)', 'C', 'Seminar', 0, 1, 1, 1, 1),
(7, 'FMI.Info.I.1.13', 'Educatie Fizica I', 'C', 'Laborator', 0, 1, 1, 1, 1),
(8, 'FMI.Info.I.2.06', 'Analiza II: Calcul diferential si integral', 'E', 'Curs', 0, 2, 1, 1, 6),
(9, 'FMI.Info.I.2.07', 'Algoritmi si structuri de date II', 'E', 'Curs', 0, 2, 1, 1, 6),
(10, 'FMI.Info.I.2.08', 'Algebra (Structuri fundamentale)', 'E', 'Curs', 0, 2, 1, 1, 6),
(11, 'FMI.Info.I.2.09', 'Geometrie analitica si diferentiala', 'E', 'Curs', 0, 2, 1, 1, 6),
(12, 'FMI.Info.I.2.10', 'Logica computationala', 'E', 'Curs', 0, 2, 1, 1, 6),
(13, 'FMI.Info.I.2.11', 'Limbi straine II (engleza)', 'C', 'Seminar', 0, 2, 1, 1, 1),
(14, 'FMI.Info.I.2.12', 'Practica la calculator', 'C', 'Laborator', 0, 2, 1, 1, 2),
(15, 'FMI.Info.I.2.13', 'Educatie Fizica II', 'C', 'Laborator', 0, 2, 1, 1, 1),
(16, 'FMI.Info.II.1.01', 'Algoritmica Grafurilor', 'E', 'Curs', 0, 1, 2, 1, 6),
(17, 'FMI.Info.II.1.02', 'Sisteme de operare', 'E', 'Curs', 0, 1, 2, 1, 6),
(18, 'FMI.Info.II.1.03', 'Limbaje formale si automate', 'E', 'Curs', 0, 1, 2, 1, 6),
(19, 'FMI.Info.II.1.04', 'Programare orientata pe obiect', 'E', 'Curs', 0, 1, 2, 1, 6),
(20, 'FMI.Info.II.1.05', 'Geometrie Computationala', 'E', 'Curs', 0, 1, 2, 1, 6),
(21, 'FMI.Info.II.1.11', 'Limbi straine', 'C', 'Seminar', 0, 1, 2, 1, 1),
(22, 'FMI.Info.II.1.13', 'Educatie fizica', 'C', 'Laborator', 0, 1, 2, 1, 1),
(23, 'FMI.Info.II.1+2.12', 'Practica la calculator', 'C', 'Seminar', 0, 2, 2, 1, 2),
(24, 'FMI.Info.II.2.06', 'Ecuatii diferentiale si cu derivate partiale', 'E', 'Curs', 0, 2, 2, 1, 5),
(25, 'FMI.Info.II.2.07', 'Probabilitati si statistica matematica', 'E', 'Curs', 0, 2, 2, 1, 5),
(26, 'FMI.Info.II.2.08', 'Calcul numeric', 'E', 'Curs', 0, 2, 2, 1, 5),
(27, 'FMI.Info.II.2.09', 'Baze de date', 'C', 'Curs', 0, 2, 2, 1, 5),
(28, 'FMI.Info.II.2.10', 'Tehnologii Web', 'C', 'Curs', 0, 2, 2, 1, 5),
(29, 'FMI.Info.II.2.11', 'Limbi straine', 'C', 'Seminar', 0, 2, 2, 1, 1),
(30, 'FMI.Info.II.2.13', 'Educatie fizica', 'C', 'Laborator', 0, 2, 2, 1, 1),
(31, 'FMI.Info.II.2.14', 'Calculabilitate si Complexitate', 'E', 'Curs', 0, 2, 2, 1, 5),
(32, 'FMI.Info.II.2.15', 'Tehnici multimedia', 'E', 'Curs', 0, 2, 2, 1, 5),
(33, 'FMI.Info.II.2.16', 'Medii vizuale de programare', 'C', 'Curs', 0, 2, 2, 1, 5),
(34, 'FMI.Info.II.2.17', 'Verificarea si validarea sistemelor soft', 'C', 'Curs', 0, 2, 2, 1, 5),
(35, 'FMI.Info.I.1.01', 'Analiza I: Calcul diferential si integral', 'C', 'Seminar', 0, 1, 1, 1, 6),
(36, '1234', 'qwerty', 'E', 'Curs', 0, 1, 1, 1, 123);

-- --------------------------------------------------------

--
-- Table structure for table `facultati`
--

CREATE TABLE `facultati` (
  `id` int(11) NOT NULL,
  `nume` varchar(255) COLLATE utf8_romanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `facultati`
--

INSERT INTO `facultati` (`id`, `nume`) VALUES
(1, 'Facultatea de Matematica si Informatica'),
(2, 'Facultatea de Medicina Dentara'),
(4, 'Facultatea de Stiinte Economice'),
(5, 'Facultatea de Istorie si Stiinte Politice'),
(6, 'Facultatea de Farmacie'),
(7, 'Facultatea de Medicina'),
(8, 'Facultatea de Psihologie si Stiintele Educatiei'),
(9, 'Facultatea de Litere'),
(10, 'Facultatea de Educatie fizica si Sport'),
(11, 'Facultatea de Teologie'),
(12, 'Facultatea de Stiinte Aplicate si Inginerie'),
(13, 'Facultatea de Arte'),
(14, 'Facultatea de Stiinte ale Naturii si Stiinte Agricole'),
(15, 'Facultatea de Drept si Stiinte Administrative'),
(16, 'Facultatea de Constructii'),
(17, 'Facultatea de Inginerie Mecanica, Industriala si Maritima'),
(19, 'e');

-- --------------------------------------------------------

--
-- Table structure for table `grupe`
--

CREATE TABLE `grupe` (
  `id` int(11) NOT NULL,
  `nume` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `serie_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `grupe`
--

INSERT INTO `grupe` (`id`, `nume`, `serie_id`) VALUES
(1, 'Grupa Mozen2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE `note` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `disciplina_id` int(11) NOT NULL,
  `nota_activitate` int(11) NOT NULL,
  `nota_examen` int(11) NOT NULL,
  `nota_finala` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `note`
--

INSERT INTO `note` (`id`, `user_id`, `disciplina_id`, `nota_activitate`, `nota_examen`, `nota_finala`) VALUES
(1, 3, 1, 8, 7, 6),
(2, 3, 4, 5, 4, 5);

-- --------------------------------------------------------

--
-- Table structure for table `serii`
--

CREATE TABLE `serii` (
  `id` int(11) NOT NULL,
  `nume` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `an` int(11) NOT NULL,
  `specializare_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `serii`
--

INSERT INTO `serii` (`id`, `nume`, `an`, `specializare_id`) VALUES
(1, 'qwerty', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `specializari`
--

CREATE TABLE `specializari` (
  `id` int(11) NOT NULL,
  `facultate_id` int(11) NOT NULL,
  `nume_specializare` varchar(255) COLLATE utf8_romanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `specializari`
--

INSERT INTO `specializari` (`id`, `facultate_id`, `nume_specializare`) VALUES
(1, 1, 'Informatica'),
(2, 1, 'Matematica'),
(3, 7, 'Medicina in limba romana'),
(4, 7, 'Medicina in limba engleza'),
(5, 7, 'Asistenta Medicala Generala'),
(6, 7, 'Balneofiziokinetoterapie si recuperare'),
(7, 2, 'Medicina dentara'),
(8, 2, 'Tehnica dentara'),
(9, 4, 'Economia firmei'),
(10, 4, 'Afaceri internationale'),
(11, 4, 'Economia comertului, turismului si serviciilor'),
(12, 4, 'Finante si banci'),
(13, 4, 'Marketing'),
(14, 4, 'Contabilitate si informatica de gestiune'),
(15, 4, 'Management'),
(16, 5, 'Stiinte politice'),
(17, 5, 'Relatii internationale si studii europene'),
(18, 5, 'Istorie'),
(19, 6, 'Farmacie'),
(20, 6, 'Asistenta de farmacie'),
(21, 8, 'Psihologie'),
(22, 8, 'Psihopedagogie speciala'),
(23, 8, 'Asistenta Sociala'),
(24, 8, 'Pedagogia invatamantului primar si prescolar'),
(25, 9, 'Limba si literatura franceza – O limba si literatura moderna (italiana / portugheza)'),
(26, 9, 'Limba si literatura franceza - O limba si literatura moderna (engleza)'),
(27, 9, 'Specializarile Limba si literatura engleza - O limba şi literatura moderna (franceza / italiana)'),
(28, 9, 'Limba si literatura engleza - O limba si literatura moderna (germana / turca / portugheza)'),
(29, 9, 'Studii americane (in limba engleza)'),
(30, 9, 'Limbi Moderne Aplicate [LMA] (Engleza - Franceza; Engleza – Italiana)'),
(31, 9, ' Limba si literatura romana - O limba si literatura modernaă (engleza / franceza / spaniola / italiana / portugheza / germana / turca)'),
(32, 9, 'Jurnalism'),
(33, 10, 'Educatie fizica si sportiva'),
(34, 10, 'Kinetoterapie si motricitate speciala'),
(35, 10, 'Sport si performanta motrica'),
(36, 11, 'Teologie ortodoxa pastorala'),
(37, 11, 'Teologie ortodoxa didactica'),
(38, 11, 'Teologie ortodoxa'),
(39, 11, 'Muzică religioasa'),
(40, 12, 'Chimie'),
(41, 12, 'Prelucrarea petrolului si petrochimie'),
(42, 12, 'Chimie alimentara si tehnologii biochimice'),
(43, 12, 'Fizica'),
(44, 12, 'Fizica tehnologica'),
(45, 12, 'Electronica aplicata'),
(46, 13, 'Artele spectacolului (actorie)'),
(47, 13, 'Artele spectacolului (coregrafie)'),
(48, 13, 'Interperare muzicala (canto)'),
(49, 13, 'Pedagogie muzicala'),
(50, 13, 'Pedagogia artelor plastice si decorative'),
(51, 14, 'Agronomie'),
(52, 14, 'Biologie'),
(53, 14, 'Geografie'),
(54, 14, 'Horticultura'),
(55, 14, 'Stiinta mediului'),
(56, 15, 'Administratie publica'),
(57, 15, 'Asistenta manageriala si secretariat'),
(58, 15, 'Drept'),
(59, 15, 'Politie locala'),
(60, 16, 'Inginerie civila'),
(61, 16, 'Inginerie si management in constructii'),
(62, 17, 'Arhitectura navala'),
(63, 17, 'Inginerie industriala'),
(64, 17, 'Inginerie mecanica'),
(65, 17, 'Inginerie energetica'),
(66, 17, 'Ingineria autovehiculelor'),
(67, 17, 'Inginerie si management'),
(73, 4, 'asad'),
(74, 1, 'qwert'),
(75, 1, 'werwer');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `cnp` varchar(14) COLLATE utf8_romanian_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `users_tip_id` int(11) NOT NULL,
  `nume` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `prenume` varchar(255) COLLATE utf8_romanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `cnp`, `password`, `users_tip_id`, `nume`, `prenume`) VALUES
(1, '1234', '1234', 1, 'Curpendin', 'Mozen'),
(3, '123', '123', 3, 'Elmas', 'Elif'),
(62, '12', '12', 3, 'a', 'b'),
(73, '01', '', 3, 'c', 'd'),
(74, '324353', '', 3, 'e', 'f'),
(75, '12389098', '', 3, '123', '123');

-- --------------------------------------------------------

--
-- Table structure for table `users_ani`
--

CREATE TABLE `users_ani` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `an` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `users_ani`
--

INSERT INTO `users_ani` (`id`, `users_id`, `an`) VALUES
(1, 1, 1),
(2, 3, 2),
(3, 62, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users_contact`
--

CREATE TABLE `users_contact` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `adresa` varchar(255) COLLATE utf8_romanian_ci DEFAULT NULL,
  `localitate` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `judet` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `tara` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `telefon` varchar(14) COLLATE utf8_romanian_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `adresa_web` varchar(255) COLLATE utf8_romanian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `users_contact`
--

INSERT INTO `users_contact` (`id`, `users_id`, `adresa`, `localitate`, `judet`, `tara`, `telefon`, `email`, `adresa_web`) VALUES
(1, 1, 'Str. Test Test', 'Constanta', 'Constanta', 'Constanta', '1234567891', 'mozen@curpedin.ro', 'www.mozen.ro'),
(70, 3, 'adr', 'loc', 'jud', 'tar', 'tel', 'email', 'adr'),
(72, 62, 'test', 'd', 'e', 'f', 'g', 'h', 'j'),
(83, 73, '', '', '', '', '', '', ''),
(84, 74, '', '', '', '', '', '', ''),
(85, 75, 'as', 'a', 'a', 'a', 'a', 'a', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `users_general`
--

CREATE TABLE `users_general` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `sex` varchar(2) COLLATE utf8_romanian_ci NOT NULL,
  `data_nasterii` date NOT NULL,
  `locul_nasterii` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `tara_nastere` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `judet_nastere` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `localitate_nastere` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `religie` varchar(255) COLLATE utf8_romanian_ci DEFAULT NULL,
  `eparhie` varchar(255) COLLATE utf8_romanian_ci DEFAULT NULL,
  `cetatenie` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `etnie` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `serie_bi_ci` varchar(10) COLLATE utf8_romanian_ci NOT NULL,
  `stare_civila` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `situatie_militara` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `nr_livret_militar` varchar(255) COLLATE utf8_romanian_ci DEFAULT NULL,
  `serie_pasaport` varchar(255) COLLATE utf8_romanian_ci DEFAULT NULL,
  `data_expirare_pasaport` date DEFAULT NULL,
  `fotografie` varchar(255) COLLATE utf8_romanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `users_general`
--

INSERT INTO `users_general` (`id`, `users_id`, `sex`, `data_nasterii`, `locul_nasterii`, `tara_nastere`, `judet_nastere`, `localitate_nastere`, `religie`, `eparhie`, `cetatenie`, `etnie`, `serie_bi_ci`, `stare_civila`, `situatie_militara`, `nr_livret_militar`, `serie_pasaport`, `data_expirare_pasaport`, `fotografie`) VALUES
(1, 1, 'M', '2016-08-03', 'Constanta', 'Romania', 'Constanta', 'Constanta', 'Musulman', 'Este', 'Roman', 'Tatar', 'KZ123123', 'Necasatorit', 'Neincorporat', NULL, NULL, NULL, ''),
(50, 62, 'M', '2016-10-18', 'abcd', 'lq', 'qw', 'we', 'rt', 'rt', 'asd', 'sdf', 'fg', 'df', 'nhg', 't', 'u', '2016-10-17', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_legaturi`
--

CREATE TABLE `users_legaturi` (
  `id` int(11) NOT NULL,
  `grupa_id` int(11) NOT NULL,
  `specializare_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users_parinti`
--

CREATE TABLE `users_parinti` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `prenume_tata` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `profesie_tata` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `loc_munca_tata` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `prenume_mama` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `profesie_mama` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `loc_munca_mama` varchar(255) COLLATE utf8_romanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `users_parinti`
--

INSERT INTO `users_parinti` (`id`, `users_id`, `prenume_tata`, `profesie_tata`, `loc_munca_tata`, `prenume_mama`, `profesie_mama`, `loc_munca_mama`) VALUES
(1, 1, 'Abv', 'asb', 'asd', 'ASD', 'ASD', 'ASD'),
(48, 62, 'a', 'a', 'a', 'a', 'aa', 'a'),
(59, 73, '', '', '', '', '', ''),
(60, 74, '', '', '', '', '', ''),
(61, 75, 'a', 'a', 'a', 'a', 'a', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `users_scolaritate`
--

CREATE TABLE `users_scolaritate` (
  `id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `categorie_studii` text COLLATE utf8_romanian_ci NOT NULL,
  `an_absolvire_liceu` int(11) NOT NULL,
  `medie_bacalaureat` double NOT NULL,
  `sesiune_admitere` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `promotie` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `tip_studii` int(11) NOT NULL,
  `forma_invatamant` int(11) NOT NULL,
  `nr_matricol` int(11) NOT NULL,
  `tip_loc` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_romanian_ci NOT NULL,
  `facultate_id` int(11) NOT NULL,
  `profil` varchar(50) COLLATE utf8_romanian_ci NOT NULL,
  `specializare_id` int(11) NOT NULL,
  `grupa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `users_scolaritate`
--

INSERT INTO `users_scolaritate` (`id`, `users_id`, `categorie_studii`, `an_absolvire_liceu`, `medie_bacalaureat`, `sesiune_admitere`, `promotie`, `tip_studii`, `forma_invatamant`, `nr_matricol`, `tip_loc`, `status`, `facultate_id`, `profil`, `specializare_id`, `grupa_id`) VALUES
(47, 62, '0', 123, 12, '12', '12', 0, 0, 0, 'p', 'p', 10, 'Domeniu: InformaticÄƒ', 17, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_tip`
--

CREATE TABLE `users_tip` (
  `id` int(11) NOT NULL,
  `tip` varchar(20) COLLATE utf8_romanian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_romanian_ci;

--
-- Dumping data for table `users_tip`
--

INSERT INTO `users_tip` (`id`, `tip`) VALUES
(1, 'Administrator'),
(2, 'Profesor'),
(3, 'Student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `discipline`
--
ALTER TABLE `discipline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `specializare_id` (`specializare_id`);

--
-- Indexes for table `facultati`
--
ALTER TABLE `facultati`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grupe`
--
ALTER TABLE `grupe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `serie_id` (`serie_id`);

--
-- Indexes for table `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `disciplina_id` (`disciplina_id`);

--
-- Indexes for table `serii`
--
ALTER TABLE `serii`
  ADD PRIMARY KEY (`id`),
  ADD KEY `specializare_id` (`specializare_id`);

--
-- Indexes for table `specializari`
--
ALTER TABLE `specializari`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facultate_id` (`facultate_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tip` (`users_tip_id`);

--
-- Indexes for table `users_ani`
--
ALTER TABLE `users_ani`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_id` (`users_id`);

--
-- Indexes for table `users_contact`
--
ALTER TABLE `users_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indexes for table `users_general`
--
ALTER TABLE `users_general`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indexes for table `users_legaturi`
--
ALTER TABLE `users_legaturi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `grupa_id` (`grupa_id`),
  ADD KEY `specializare_id` (`specializare_id`);

--
-- Indexes for table `users_parinti`
--
ALTER TABLE `users_parinti`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indexes for table `users_scolaritate`
--
ALTER TABLE `users_scolaritate`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `specializare_id` (`specializare_id`),
  ADD UNIQUE KEY `grupa_id` (`grupa_id`),
  ADD UNIQUE KEY `facultate_id` (`facultate_id`),
  ADD KEY `users_id` (`users_id`);

--
-- Indexes for table `users_tip`
--
ALTER TABLE `users_tip`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `discipline`
--
ALTER TABLE `discipline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `facultati`
--
ALTER TABLE `facultati`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `grupe`
--
ALTER TABLE `grupe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `note`
--
ALTER TABLE `note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `serii`
--
ALTER TABLE `serii`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `specializari`
--
ALTER TABLE `specializari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `users_ani`
--
ALTER TABLE `users_ani`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users_contact`
--
ALTER TABLE `users_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `users_general`
--
ALTER TABLE `users_general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `users_legaturi`
--
ALTER TABLE `users_legaturi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users_parinti`
--
ALTER TABLE `users_parinti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `users_scolaritate`
--
ALTER TABLE `users_scolaritate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `users_tip`
--
ALTER TABLE `users_tip`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `discipline`
--
ALTER TABLE `discipline`
  ADD CONSTRAINT `FK_disciplina_specializari` FOREIGN KEY (`specializare_id`) REFERENCES `specializari` (`id`);

--
-- Constraints for table `grupe`
--
ALTER TABLE `grupe`
  ADD CONSTRAINT `FK_grupe_serii` FOREIGN KEY (`serie_id`) REFERENCES `serii` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `FK_note_discipline` FOREIGN KEY (`disciplina_id`) REFERENCES `discipline` (`id`),
  ADD CONSTRAINT `FK_note_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `serii`
--
ALTER TABLE `serii`
  ADD CONSTRAINT `FK_serii_specializari` FOREIGN KEY (`specializare_id`) REFERENCES `specializari` (`id`);

--
-- Constraints for table `specializari`
--
ALTER TABLE `specializari`
  ADD CONSTRAINT `FK_specializari_facultati` FOREIGN KEY (`facultate_id`) REFERENCES `facultati` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `FK_users_users_tip` FOREIGN KEY (`users_tip_id`) REFERENCES `users_tip` (`id`);

--
-- Constraints for table `users_ani`
--
ALTER TABLE `users_ani`
  ADD CONSTRAINT `FK_users_ani_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users_contact`
--
ALTER TABLE `users_contact`
  ADD CONSTRAINT `FK_users_contact_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_general`
--
ALTER TABLE `users_general`
  ADD CONSTRAINT `FK_users_general_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_legaturi`
--
ALTER TABLE `users_legaturi`
  ADD CONSTRAINT `FK_users_legaturi_grupe` FOREIGN KEY (`grupa_id`) REFERENCES `grupe` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_users_legaturi_specializari` FOREIGN KEY (`specializare_id`) REFERENCES `specializari` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_parinti`
--
ALTER TABLE `users_parinti`
  ADD CONSTRAINT `FK_users_parinti_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users_scolaritate`
--
ALTER TABLE `users_scolaritate`
  ADD CONSTRAINT `FK_users_scolaritate_facultati` FOREIGN KEY (`facultate_id`) REFERENCES `facultati` (`id`),
  ADD CONSTRAINT `FK_users_scolaritate_grupe` FOREIGN KEY (`grupa_id`) REFERENCES `grupe` (`id`),
  ADD CONSTRAINT `FK_users_scolaritate_specializari` FOREIGN KEY (`specializare_id`) REFERENCES `specializari` (`id`),
  ADD CONSTRAINT `FK_users_scolaritate_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
