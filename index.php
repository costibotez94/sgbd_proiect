<!DOCTYPE html>
<html>
<head>
	<title>PROIECT SGBD</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
	<?php ini_set('display_errors', 1); ?>
	<!-- INCLUDE LIBRARIE -->
		<?php require_once 'librarii/manager.php'; ?>
	<div class="container">
		<!-- HEADER -->
		<div class="row">
			<div class="col-md-6 logo">
				<a href="/sgbd">
					<img src="images\database.png" alt="UMS" title="Licenta Mozen" height="100" width="100"/>
				</a>
			</div>
			<div class="col-md-6 title">
			<h2><center><b><span class="label label-default">Proiect</span></b></center>
			<center><p>Sistem de Gestiune a Bazelor de Date</center></h2>
				<!-- <h2> <center><b>Proiect</b></center>
				<p>Sistem de Gestiune a Bazelor de Date </h2> -->
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
				  Ajutor
				</button>

				<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">Ajutor</h4>
				      </div>
				      <div class="modal-body">
				        ...
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
		<!-- BODY -->
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-primary">
					<div class="panel-heading"><strong>ADAUGARE</strong></div>
					<div class="panel-body">
						<a class="btn btn-default" href="?action=adaugare&model=adaugare-baza-de-date">Adauga baza de date</a>
						<a class="btn btn-default" href="?action=adaugare&model=adaugare-tabel">Adauga tabel</a>
						<a class="btn btn-default" href="?action=adaugare&model=adaugare-rand">Adauga rand</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-warning">
					<div class="panel-heading"><strong>EDITARE<strong></div>
					<div class="panel-body">
						<a class="btn btn-default" href="?action=editare&model=editare-baza-de-date">Editeaza baza de date</a>
						<a class="btn btn-default" href="?action=editare&model=editare-tabel">Editeaza tabel</a>
						<a class="btn btn-default" href="?action=editare&model=editare-rand">Editeaza rand</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-danger">
					<div class="panel-heading"><strong>STERGERE</strong></div>
					<div class="panel-body">
						<a class="btn btn-default" href="?action=stergere&model=stergere-baza-de-date">Sterge baza de date</a>
						<a class="btn btn-default" href="?action=stergere&model=stergere-tabel">Sterge tabel</a>
						<a class="btn btn-default" href="?action=stergere&model=stergere-rand">Sterge rand</a>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="panel panel-success">
					<div class="panel-heading"><strong>LISTARE</strong></div>
					<div class="panel-body">
						<a class="btn btn-default" href="?action=listare&model=listare-baze-de-date">Listare baze de date</a>
						<a class="btn btn-default" href="?action=listare&model=listare-tabele">Listare tabele</a>
						<a class="btn btn-default" href="?action=listare&model=listare-randuri">Listare randuri</a>
					</div>
				</div>
			</div>
		</div>
		<!-- TEMPLATE -->
		<div class="row">
			<?php
				if($_GET)
				{
					if($_GET['action'] == 'adaugare') {
						switch ($_GET['model']) {
							case 'adaugare-baza-de-date':
								include_once 'sabloane/adaugare/adaugare_baza_de_date.php';
								break;
							case 'adaugare-tabel':
								include_once 'sabloane/adaugare/adaugare_tabel.php';
								break;
							case 'adaugare-rand':
								include_once 'sabloane/adaugare/adaugare_rand.php';
								break;
						}
					}
					if($_GET['action'] == 'editare') {
						switch ($_GET['model']) {
							case 'editare-baza-de-date':
								include_once 'sabloane/editare/editare_baza_de_date.php';
								break;
							case 'editare-tabel':
								include_once 'sabloane/editare/editare_tabel.php';
								break;
							case 'editare-rand':
								include_once 'sabloane/editare/editare_rand.php';
								break;
						}
					}
					if($_GET['action'] == 'stergere') {
						switch ($_GET['model']) {
							case 'stergere-baza-de-date':
								include_once 'sabloane/stergere/stergere_baza_de_date.php';
								break;
							case 'stergere-tabel':
								include_once 'sabloane/stergere/stergere_tabel.php';
								break;
							case 'stergere-rand':
								include_once 'sabloane/stergere/stergere_rand.php';
								break;
						}
					}
					if($_GET['action'] == 'listare') {
						switch ($_GET['model']) {
							case 'listare-baze-de-date':
								include_once 'sabloane/listare/listare_baze_de_date.php';
								break;
							case 'listare-tabele':
								include_once 'sabloane/listare/listare_tabele.php';
								break;
							case 'listare-randuri':
								include_once 'sabloane/listare/listare_randuri.php';
								break;
						}
					}
				}
			?>
			</div>
	</div>
</body>
	<footer>
		<p>© Curpedin Mozen - 2016</p>
	</footer>
</html>